import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteProtectionComponent } from './complete-protection.component';

describe('CompleteProtectionComponent', () => {
  let component: CompleteProtectionComponent;
  let fixture: ComponentFixture<CompleteProtectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteProtectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteProtectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
