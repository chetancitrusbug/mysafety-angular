import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStartFreeTrialComponent } from './footer-start-free-trial.component';

describe('FooterStartFreeTrialComponent', () => {
  let component: FooterStartFreeTrialComponent;
  let fixture: ComponentFixture<FooterStartFreeTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStartFreeTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStartFreeTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
