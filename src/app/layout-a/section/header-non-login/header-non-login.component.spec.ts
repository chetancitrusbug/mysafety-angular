import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderNonLoginComponent } from './header-non-login.component';

describe('HeaderNonLoginComponent', () => {
  let component: HeaderNonLoginComponent;
  let fixture: ComponentFixture<HeaderNonLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderNonLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderNonLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
