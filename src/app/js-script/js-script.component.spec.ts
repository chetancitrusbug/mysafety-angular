import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JsScriptComponent } from './js-script.component';

describe('JsScriptComponent', () => {
  let component: JsScriptComponent;
  let fixture: ComponentFixture<JsScriptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JsScriptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JsScriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
