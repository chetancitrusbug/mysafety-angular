import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { TextMaskModule } from 'angular2-text-mask';

import { AppComponent } from './app.component';
import { HomeComponent } from './layout-a/home/home.component';
import { HeaderStartFreeTrialComponent } from './layout-a/section/header-start-free-trial/header-start-free-trial.component';
import { HeaderNonLoginComponent } from './layout-a/section/header-non-login/header-non-login.component';
import { FeaturesComponent } from './layout-a/section/features/features.component';
import { CompleteProtectionComponent } from './layout-a/section/complete-protection/complete-protection.component';
import { HowItWorkComponent } from './layout-a/section/how-it-work/how-it-work.component';
import { PricingPlansComponent } from './layout-a/section/pricing-plans/pricing-plans.component';
import { LatestNewsComponent } from './layout-a/section/latest-news/latest-news.component';
import { FooterStartFreeTrialComponent } from './layout-a/section/footer-start-free-trial/footer-start-free-trial.component';
import { FooterComponent } from './layout-a/section/footer/footer.component';
import { JsScriptComponent } from './js-script/js-script.component';
import { BlogDetailComponent } from './layout-a/blog-detail/blog-detail.component';
import { BlogListingComponent } from './layout-a/blog-listing/blog-listing.component';
import { ForgotpasswordComponent } from './layout-a/forgotpassword/forgotpassword.component';
import { SignInComponent } from './layout-a/sign-in/sign-in.component';
import { SignUpComponent } from './layout-a/sign-up/sign-up.component';

import { UnauthModule } from './layout-a/unauth.module';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderStartFreeTrialComponent,
    HeaderNonLoginComponent,
    FeaturesComponent,
    CompleteProtectionComponent,
    HowItWorkComponent,
    PricingPlansComponent,
    LatestNewsComponent,
    FooterStartFreeTrialComponent,
    FooterComponent,
    JsScriptComponent,
    BlogDetailComponent,
    BlogListingComponent,
    ForgotpasswordComponent,
    SignInComponent,
    SignUpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CurrencyMaskModule,
    TextMaskModule,
    UnauthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
