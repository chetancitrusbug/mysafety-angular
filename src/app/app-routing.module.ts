import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './layout-a/home/home.component';   
import { UnauthModule } from './layout-a/unauth.module';   

const routes: Routes = [
	{
    path: 'admin',
		loadChildren: 'app/layout-a/unauth.module#UnauthModule'
	},
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
